package com.afs.restapi.service;

import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee newEmployee) {
        if (newEmployee.getAge() < 18 || newEmployee.getAge() > 65) {
            throw new InvalidEmployeeException();
        }
        return employeeRepository.insert(newEmployee);
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long employeeId) {
        return employeeRepository.findById(employeeId);
    }

    public List<Employee> findByGender(String employeeGender) {
        return employeeRepository.findByGender(employeeGender);
    }

    public List<Employee> findByPage(Integer pageNumber, Integer pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public void delete(Long employeeId) {
        employeeRepository.delete(employeeId);
    }

    public Employee update(Long employeeId, Employee employeeInfo) {
        if (!employeeRepository.findById(employeeId).isActive()) {
            throw new InactiveEmployeeException();
        }
        return employeeRepository.update(employeeId, employeeInfo);
    }
}
