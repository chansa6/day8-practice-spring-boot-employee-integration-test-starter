package com.afs.restapi;


import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;



    @Test
    void should_throw_invalid_employee_exception_when_create_given_employee_age_below_18() {
        Employee newEmployee = new Employee("Jerry", 15, "Male", 999);
        assertThrows(InvalidEmployeeException.class, () -> employeeService.create(newEmployee));
    }

    @Test
    void should_throw_invalid_employee_exception_when_create_given_employee_age_above_65() {
        Employee newEmployee = new Employee("Jamie", 66, "Male", 222);
        assertThrows(InvalidEmployeeException.class, () -> employeeService.create(newEmployee));
    }

    @Test
    void should_add_new_employee_when_create_given_employee_age_in_range() {
        Employee newEmployee = new Employee("Jerry", 45, "Male", 9999);
        employeeService.create(newEmployee);
        verify(employeeRepository, times(1)).insert(newEmployee);
    }

    @Test
    void should_get_all_employees_when_find_all_given_employees() {
        employeeService.findAll();
        verify(employeeRepository, times(1)).findAll();
    }


    @Test
    void should_get_specific_employee_when_find_by_id_given_employee_id() {
        Long employeeId = 2L;
        employeeService.findById(employeeId);
        verify(employeeRepository, times(1)).findById(employeeId);
    }

    @Test
    void should_get_employees_of_gender_when_find_by_gender_given_gender() {
        String employeeGender = "Male";
        employeeService.findByGender(employeeGender);
        verify(employeeRepository, times(1)).findByGender(employeeGender);
    }

    @Test
    void should_get_employees_of_page_when_find_by_page_given_page_num_and_page_size() {
        Integer pageNumber = 2;
        Integer pageSize = 2;
        employeeService.findByPage(pageNumber, pageSize);
        verify(employeeRepository, times(1)).findByPage(pageNumber, pageSize);
    }

    @Test
    void should_delete_employee_when_delete_given_employee_id() {
        Long employeeId = 1L;
        employeeService.delete(employeeId);
        verify(employeeRepository, times(1)).delete(employeeId);
    }

    @Test
    void should_throw_inactive_employee_exception_when_update_give_inactive_employee() {
        Long employeeId = 1L;
        Employee employee = new Employee(employeeId, "Jerry", 46, "Male", 500);
        Employee employeeNewInfo = new Employee(employeeId, "Jerry", 47, "Male", 800);
        when(employeeRepository.findById(employeeId)).thenReturn(employee);
        employeeRepository.findById(employeeId).setActive(false);

        assertThrows(InactiveEmployeeException.class, () -> employeeService.update(employeeId, employeeNewInfo));
    }

    @Test
    void should_update_employee_when_update_given_active_employee() {
        Long employeeId = 1L;
        Employee employee = new Employee(employeeId, "Jerry", 46, "Male", 500);
        Employee employeeNewInfo = new Employee( "Jerry", 47, "Male", 800);
        when(employeeRepository.findById(employeeId)).thenReturn(employee);
        employeeService.update(employeeId, employeeNewInfo);

        verify(employeeRepository, times(1)).update(employeeId, employeeNewInfo);
    }


}
