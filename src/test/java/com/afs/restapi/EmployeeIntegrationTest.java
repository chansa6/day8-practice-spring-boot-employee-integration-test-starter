package com.afs.restapi;


import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeIntegrationTest {

    @Autowired
    MockMvc postmanMock;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    EmployeeRepository employeeRepository;


    @BeforeEach
    void setup() {
        employeeRepository.reset();
    }

    @Test
    void should_return_all_5_employees_when_get_all_given_resource_has_5_employees() throws Exception {
        postmanMock.perform((MockMvcRequestBuilders.get("/employees")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(5)));
    }

    @Test
    void should_return_employee_3_when_get_employee_by_id_given_resource_has_employee_3() throws Exception {
        postmanMock.perform((MockMvcRequestBuilders.get("/employees/3")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(3L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("David Williams"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(35))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5500));
    }

    @Test
    void should_return_5_employees_and_right_inactive_status_when_delete_employee_given_resource_has_5_active_employees() throws Exception {
        postmanMock.perform((MockMvcRequestBuilders.delete("/employees/2")))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        postmanMock.perform((MockMvcRequestBuilders.get("/employees")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(5)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].active").value(false));
    }

    @Test
    void should_return_male_employees_when_perform_get_given_employee_gender() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employees")
                .param("gender", "Male");

        postmanMock.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$..gender")
                        .value(everyItem(is("Male"))));
    }

    @Test
    void should_return_right_number_of_employees_when_perform_get_by_page_given_employees_page_num_page_size() throws Exception {
        List<Employee> employeeListMock = new ArrayList<>();
        employeeListMock.add(new Employee(3L, "David Williams", 35,"Male", 5500));
        employeeListMock.add(new Employee(4L, "Emily Brown", 23, "Female", 4500));
        String employeeListMockString = objectMapper.writeValueAsString(employeeListMock);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employees")
                .param("pageNumber", String.valueOf(2))
                .param("pageSize", String.valueOf(2));

        postmanMock.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(result -> assertEquals(employeeListMockString, result.getResponse().getContentAsString()));

    }

    @Test
    void should_return_new_active_employee_when_perform_post_given_new_employee() throws Exception {
        Employee employeeMockToAdd = new Employee( "Test", 35,"Male", 1);
        Employee employeeMockForCompare = new Employee(6L, "Test", 35,"Male", 1);
        String employeeMockToAddString = objectMapper.writeValueAsString(employeeMockToAdd);
        String employeeMockForCompareString = objectMapper.writeValueAsString(employeeMockForCompare);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeMockToAddString);

        postmanMock.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(result -> assertEquals(employeeMockForCompareString, result.getResponse().getContentAsString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.active").value(true));

        postmanMock.perform(MockMvcRequestBuilders.get("/employees/6"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(result -> assertEquals(employeeMockForCompareString, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_updated_active_employee_when_update_employee_given_resource_has_right_employee_and_new_update_data() throws Exception {
        Employee mockEmployee = new Employee(2L, "Jane Johnson", 5, "Female", 10);
        String mockEmployeeString = objectMapper.writeValueAsString(mockEmployee);

        postmanMock.perform(MockMvcRequestBuilders.put("/employees/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mockEmployeeString))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.active").value(true))
                .andExpect(result -> assertEquals(mockEmployeeString, result.getResponse().getContentAsString()));

    }

    @Test
    void should_return_404_when_perform_get_by_id_given_employee_not_exist() throws Exception {
        postmanMock.perform(MockMvcRequestBuilders.get("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_return_404_when_perform_delete_by_id_given_employee_not_exist() throws Exception {
        postmanMock.perform(MockMvcRequestBuilders.delete("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_return_404_when_perform_put_by_id_given_employee_not_exist() throws Exception {
        Employee mockEmployee = new Employee(2L, "Jane Johnson", 5, "Female", 10);
        String mockEmployeeString = objectMapper.writeValueAsString(mockEmployee);

        postmanMock.perform(MockMvcRequestBuilders.put("/employees/999")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mockEmployeeString))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

}
